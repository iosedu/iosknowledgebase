# README #

### What is this repository for? ###

* Здесь будут собраны все вопросы, ответы, тестовые задания, code snippets для повышения знаний в iOS и прохождения собеседований.
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo) или [markdown-cheatsheet-online](https://guides.github.com/pdfs/markdown-cheatsheet-online.pdf)
* [Live reload for md files](https://dillinger.io)
* [Atom.io](https://atom.io) editor with [markdown-preview package](https://atom.io/packages/markdown-preview)

### Contribution guidelines ###

* НЕ редакртируйте файлы в online редакторе
* Вносите изменнения через pull request
* Участвуйте в ревью
* Для вопросов или тем используйте "*", для ответов ">" знаки
* Если ответ на вопрос или тему занимает пару предложений, можно писать ниже используя ">" знак,
если больше, создаем новый md файл с описанием темы или ответом и оставляем под вопросом ссылку на него.

### Who do I talk to? ###

* [@thestaryaprod](https://t.me/thestaryaprod)
* [@dee_lv](https://t.me/dee_lv)
* [Community](https://t.me/joinchat/F_--ixpPIVAKKUacv3IJKg)

### Navigation

* [QA](https://bitbucket.org/iosedu/iosknowledgebase/src/master/QA.md)
* [Test Tasks](https://bitbucket.org/iosedu/iosknowledgebase/src/master/TestTasks/TasksList.md)
* [Useful links (aka что посмотреть)](https://bitbucket.org/iosedu/iosknowledgebase/src/master/Useful%20links%20(aka%20что%20посмотреть%3F).md)
