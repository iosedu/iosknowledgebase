### Список тестовых заданий

* [8-Ball](https://bitbucket.org/iosedu/iosknowledgebase/src/master/TestTasks/8-Ball.md)
* [GitHub users](https://bitbucket.org/iosedu/iosknowledgebase/src/master/TestTasks/GitHub%20users.md)
* [Менеджер финансов](https://bitbucket.org/iosedu/iosknowledgebase/src/master/TestTasks/Менеджер%20финансов.md)
* [Приложение для поиска GIF](https://bitbucket.org/iosedu/iosknowledgebase/src/master/TestTasks/Приложение%20для%20поиска%20GIF.md)
* [Прогноз погоды 3 in 1 ](https://bitbucket.org/iosedu/iosknowledgebase/src/master/TestTasks/Прогноз%20погоды.md)
* [Список популярных картинок](https://bitbucket.org/iosedu/iosknowledgebase/src/master/TestTasks/Список%20популярных%20картинок.md)
* [Финансовый трекер](https://bitbucket.org/iosedu/iosknowledgebase/src/master/TestTasks/Финансовый%20трекер.md)
