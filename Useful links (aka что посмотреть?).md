# Ссылки на интересные и полезные видео (уроки, подкасты, лекции)

### Видео

* SOLID принципы
    - [Архитектура. SOLID [RU] / Мобильный разработчик](https://www.youtube.com/watch?v=ILdVbKs6qYg&list=PLLaP-ndsvzssT7ODYju94A-HoShR-jozP&index=10&t=0s)
    - [SOLID принципы в объектно ориентированном программировании](https://www.youtube.com/watch?v=47-F0wGz-Vk&list=PLLaP-ndsvzssT7ODYju94A-HoShR-jozP&index=8&t=0s)
	- [SOLID [RU] / Ildar Zalyalov](https://youtu.be/QMqUrn9GIG0?t=1836)
	- [Кто подставил Барбару Лисков, или кто кого SOLID / Сергей Крапивенский](https://www.youtube.com/watch?v=h2EKBLTRpi4&t=1614s)

* Design patterns
    - [Шаблоны проектирования (playlist)](https://www.youtube.com/watch?v=-bL5MtM9_nQ&list=PLDYxpD3lDWsxWls2_-mwOHLGdtrpqapVJ)
	
* Алгоритмы на языке Swift
	- Решение задач на языке Swift 5.3 / Ian Solomein (https://www.youtube.com/playlist?list=PLUb9K99oQb2uTprOy3V9E1fXpydcMmTRK)
	- Swift алгоритмы / Easy Swift (https://www.youtube.com/playlist?list=PLFCdvmRFNii9SpEyylCtbqwG0eUfxTmOS)
	


### Статьи

* Как готовиться к интервью
    - [Navigating the iOS Interview](https://www.raywenderlich.com/10625296-navigating-the-ios-interview)
* Алгоритмы и структуры данных
	- [Swift Algorithm Club от Ray Wenderlich](https://github.com/raywenderlich/swift-algorithm-club)
	
### Приложения

* [Algorithms & Data Structures / Alexander Murphy](https://apps.apple.com/us/app/algorithms-data-structures/id1431032601)
* [Swifty-Quiz - Проверь свои знания в Swift](https://apps.apple.com/ru/app/swifty-quiz/id1525844750)

### Книги
* git
    - [Pro Git / Scott Chacon and Ben Straub](https://git-scm.com/book/en/v2)
